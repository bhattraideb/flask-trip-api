## Flask Trip App API 
This REST API is developed using:

 - Python/Flask framework
 - PostgreSQL
 - Nginx/Gunicorn  
 - Docker    

## Getting started

To start project in local environment:
    - In terminal window, run below commands from root of the project:

```
- docker-compose up -d --build
- docker-compose exec web python manage.py create_db
```
The application will then be available at http://127.0.0.1:5000

To run the application in production environment, run the following commands:

```
- docker-compose -f docker-compose.prod.yml up -d --build
- docker-compose -f docker-compose.prod.yml exec web python manage.py creat
```
The application will then be available at http://0.0.0.0:4000

Verify application is running from your browser with respective environment URL.

To test the APIs, go to postman verify the below endpoints are working perfectly.

1. **Register user:   {{URL}}/register**
2. **User Login:      {{URL}}/login**
3. **Token refresh:   {{URL}}/refresh-token**
4. **Add city:        {{URL}}/city**
5. **City List:       {{URL}}/citylist**
6. **Add Trip:        {{URL}}/trip**
7. **Trip List:       {{URL}}/triplist**
8. **Search trip      {{URL}}/search/{source city:'MBI'}/{destination city: 'DLI'}**

I have tested these in both production and local environments. 

You can find the complete documentation of the APIs [here](https://gitlab.com/bhattraideb/flask-trip-api/-/blob/master/README.md)

## ToDos
1. Organize files into seeparate dev/prod environments 
2. Use terraform to create resources on cloud(AWS)
3. Optimize the trip search query
4. Optimize the search result