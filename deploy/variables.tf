variable "prefix" {
  default = "trip"
}

variable "project" {
  default = "trip-api"
}
variable "contact" {
  default = "bhatrai.deb@gmail.com"
}
variable "db_username" {
  description = "Usernname for the RDS postgres instance"
}
variable "db_password" {
  description = "Password for the RDS postgres instance"
}
variable "bastion_key_name" {
  default = "trip-api-devops-key-pair"
}
variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "645662440846.dkr.ecr.us-east-1.amazonaws.com/trip-api-devops:latest"
}

