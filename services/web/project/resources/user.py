from flask_restful import Resource, reqparse
from werkzeug.security import safe_str_cmp
from project.blacklist import BLACKLIST
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    get_jwt_identity,
    get_jwt
)

from project.models.user import UserModel

_user_parser = reqparse.RequestParser()
_user_parser.add_argument('username',
                          type=str,
                          required=True,
                          help="Username field cannot be left blank!"
                          )

_user_parser.add_argument('password',
                          type=str,
                          required=True,
                          help="Password field cannot be left blank!"
                          )


class UserRegister(Resource):
    def post(self):
        data = _user_parser.parse_args()

        if UserModel.find_by_username(data['username']):
            return {"message": "User with that username already exists."}, 400

        user = UserModel(**data)
        user.save_to_db()

        return {"message": "User created successfully."}, 201


# class User(Resource):
#     @classmethod
#     def get(self, user_id):
#         user = UserModel.find_by_id(user_id)
#         if not user:
#             return {'messase': 'User not found'}, 404
#
#         return user.json()

# @classmethod
# def delete(self, user_id):
#     user = UserModel.find_by_id(user_id)
#     if not user:
#         return {'messase': 'User not found'}, 404
#     user.delete_from_db()
#     return {'message': 'User deleted'}, 200


# class UserList(Resource):
#     @classmethod
#     def get(self):
#         users = UserModel.find_all()
#         if not users:
#             return {'messase': 'Users not found'}, 404
#
#         return {'users': [user.json() for user in users]}, 200


class UserLogin(Resource):
    @classmethod
    def post(cls):
        data = _user_parser.parse_args()
        user = UserModel.find_by_username(data['username'])
        if user and safe_str_cmp(user.password, data['password']):
            access_token = create_access_token(identity=user.id, fresh=True)
            refresh_token = create_refresh_token(user.id)
            return {'access_token': access_token, 'refresh_token': refresh_token}, 200
        return {'message': 'Invalid credentials!'}, 401


class UserLogOut(Resource):
    @jwt_required()
    def post(self):
        jti = get_jwt()['jti']
        BLACKLIST.add(jti)
        return {'message': 'Successfully logout!'}, 200


class TokenRefresh(Resource):
    @jwt_required(refresh=True)
    def post(self):
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user, fresh=False)
        return {'access_token': new_token}, 200
