from flask_restful import Resource, reqparse
from project.models.cities import CityModel

_city_parser = reqparse.RequestParser()
_city_parser.add_argument('name',
                          type=str,
                          required=True,
                          help="City name field cannot be left blank!"
                          )

_city_parser.add_argument('code',
                          type=str,
                          required=True,
                          help="City code field cannot be left blank!"
                          )

_city_parser.add_argument('longitude',
                          type=str,
                          required=True,
                          help="City longitude field cannot be left blank!"
                          )

_city_parser.add_argument('lattitude',
                          type=str,
                          required=True,
                          help="City lattitude field cannot be left blank!"
                          )


class City(Resource):
    def get(self, name):
        city = CityModel.find_by_name(name)
        if city:
            return city.json()
        else:
            return {'message': 'city not found'}, 404

    def post(self):
        data = _city_parser.parse_args()

        if CityModel.find_by_name(data['name']):
            return {'message': 'A city with name {} already exists'.format(data['name'])}, 400

        city = CityModel(**data)

        try:
            city.save_to_db()
        except InterruptedError:
            return {'message': 'An error occured while creating the city'}, 500

        return city.json(), 201


class CityList(Resource):
    def get(self):
        return {'cities': list(map(lambda x: x.json(), CityModel.find_all()))}
