from flask_restful import Resource, reqparse
from project.models.cities import CityModel
from project.models.trips import TripModel

_trip_parser = reqparse.RequestParser()
_trip_parser.add_argument('source_id',
                          type=str,
                          required=True,
                          help="Source city name field cannot be left blank!"
                          )

_trip_parser.add_argument('destination_id',
                          type=str,
                          required=True,
                          help="Destination city code field cannot be left blank!"
                          )

_trip_parser.add_argument('medium',
                          type=str,
                          required=True,
                          help="Transport Medium field cannot be left blank!"
                          )

_trip_parser.add_argument('departure',
                          type=str,
                          required=True,
                          help="Departure datetime field cannot be left blank!"
                          )

_trip_parser.add_argument('arrival',
                          type=str,
                          required=True,
                          help="Arrival datetime field cannot be left blank!"
                          )


class Trip(Resource):
    # def get(self, name):
    #     trip = TripModel.find_by_name(name)
    #     if trip:
    #         return trip.json()
    #     else:
    #         return {'message': 'store not found'}, 404

    def post(self):
        data = _trip_parser.parse_args()

        trip = TripModel(**data)

        try:
            trip.save_to_db()
        except UnboundLocalError:
            return {'message': 'An error occured while creating the store'}, 500

        return trip.json(), 201

    # def delete(self, name):
    #     trip = TripModel.find_by_name(name)
    #     if trip:
    #         trip.delete_from_db()
    #
    #     return {'message': 'Store deleted'}


class SearchTrip(Resource):
    def get(self, source, destination):

        source_id = CityModel.find_by_code(source)
        destination_id = CityModel.find_by_code(destination)

        if source_id and destination_id:
            return TripModel.search_trip(source_id.id, destination_id.id)
        else:
            return {'message': 'No service available for this source/destination'}


class TripList(Resource):
    def get(self):
        return {'trips': list(map(lambda x: x.json(), TripModel.find_all()))}
