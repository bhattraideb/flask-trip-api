from project.db import db


class TripModel(db.Model):
    __tablename__ = "trips"

    id = db.Column(db.Integer, primary_key=True)
    source_id = db.Column(db.Integer, db.ForeignKey("cities.id"), nullable=False)
    destination_id = db.Column(db.Integer, db.ForeignKey("cities.id"), nullable=False)

    source = db.relationship(
        "CityModel", foreign_keys="TripModel.source_id", lazy='select',
    )
    destination = db.relationship(
        "CityModel", foreign_keys="TripModel.destination_id", lazy='select',
    )

    medium = db.Column(db.String(80), nullable=False)
    departure = db.Column(db.String(80), nullable=False)
    arrival = db.Column(db.String(80), nullable=False)

    def __init__(self, source_id, destination_id, medium, departure, arrival):
        self.source_id = source_id
        self.destination_id = destination_id
        self.medium = medium
        self.departure = departure
        self.arrival = arrival

    def json(self):
        return {
            'id': self.id,
            'source_id': self.source_id,
            'source': self.source.json(),
            'destination': self.destination.json(),
            'destination_id': self.destination_id,
            'departure': self.departure,
            'arrival': self.arrival
        }

    # @classmethod
    # def find_by_name(cls, name):
    #     return cls.query.filter_by(name=name).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    # def delete_from_db(self):
    #     db.session.delete(self)
    #     db.session.commit()

    @classmethod
    def search_trip(cls, source_id, destination_id):
        trips = cls.query.filter_by(source_id=int(source_id), destination_id=int(destination_id))
        if trips:
            return {'trips': [trip.json() for trip in trips]}, 200
        else:
            return {'message': 'No trip found for this route'}, 404
