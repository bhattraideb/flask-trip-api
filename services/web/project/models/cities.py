from project.db import db


class CityModel(db.Model):
    __tablename__ = "cities"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    code = db.Column(db.String(80), nullable=False)
    longitude = db.Column(db.Float(precision=6), nullable=False)
    lattitude = db.Column(db.Float(precision=6), nullable=False)

    def __init__(self, name, code, longitude, lattitude):
        self.name = name
        self.code = code
        self.longitude = longitude
        self.lattitude = lattitude

    def json(self):
        return {
            'id': self.id,
            'name': self.name,
            'code': self.code,
            'longitude': self.longitude,
            'lattitude': self.lattitude
        }

    # @classmethod
    # def find_by_name(cls, name):
    #     return cls.query.filter_by(name=name).first()

    @classmethod
    def find_by_code(cls, code):
        return cls.query.filter_by(code=code).first()

    @classmethod
    def find_all(cls):
        return cls.query.all()

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    # def delete_from_db(self):
    #     db.session.delete(self)
    #     db.session.commit()
