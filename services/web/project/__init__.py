import os
from flask import (
    Flask,
    jsonify,
    send_from_directory,
    request,
    redirect,
    url_for
)
from flask_restful import Api
from flask_jwt_extended import JWTManager
from project.db import db

from project.resources.user import (
    UserRegister,
    UserLogin,
    UserLogOut,
    TokenRefresh
)

from project.resources.cities import City, CityList
from project.resources.trips import Trip, TripList, SearchTrip

app = Flask(__name__)
app.config.from_object("project.config.Config")
jwt = JWTManager(app)
api = Api(app)
db.init_app(app)


@app.route("/")
def hello_world():
    return jsonify(message="Testing web application")


@app.route("/media/<path:filename>")
def mediafiles(filename):
    return send_from_directory(app.config["MEDIA_FOLDER"], filename)


@app.route("/static/<path:filename>")
def staticfiles(filename):
    return send_from_directory(app.config["STATIC_FOLDER"], filename)


api.add_resource(UserRegister, '/register')
# api.add_resource(User, '/user/<int:user_id>')
# api.add_resource(UserList, '/users')
api.add_resource(UserLogin, '/login')
api.add_resource(TokenRefresh, '/refresh-token')
api.add_resource(UserLogOut, '/logout')

api.add_resource(City, '/city')
api.add_resource(CityList, '/citylist')

api.add_resource(Trip, '/trip')
api.add_resource(TripList, '/triplist')

api.add_resource(SearchTrip, '/search/<string:source>/<string:destination>')
